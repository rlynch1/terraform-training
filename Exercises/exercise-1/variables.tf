#############
# Variables #
#############
variable "key_name" {
  description = "Name of your key"
  type        = string
  default     = "alias/data-encryption/mykey"
}
