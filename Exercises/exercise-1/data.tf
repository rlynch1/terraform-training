################
# Data Sources #
################
data "aws_caller_identity" "current" {}

#####################
# Local Definitions #
#####################
# Don't update
locals {
  time          = timestamp()
  creation_date = formatdate("DD MMM YYYY hh:mm ZZZ", local.time)

  mandatory_tags = map(
    "CREATION-DATE", local.creation_date,
    "CREATED-BY", "TERRAFORM",
    "DEPLOYED-BY", data.aws_caller_identity.current.user_id
  )
}
