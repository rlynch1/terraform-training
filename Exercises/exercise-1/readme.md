# Exercise 1 - Variables, Resources and Outputs

Ensure that you have logged into the AWS account before you start the exercise.

## Part 1 - Variables 

A JIRA ticket with ticket number CE-123 has been assigned to you. The ticket entails that you need to create a KMS key with an appropriate name. This key will be used for data encryption. The client also wants the category of the key(data/service/app-encryption etc) to be included in the key name so the key name should look along the lines of 'alias/data-encryption/mykey'. Deploy a KMS key in the account to successfully complete and close your JIRA ticket. 

### Steps 

* Ensure that you are in the exercise-1 directory within your command line interface

* In your IDE, open the .tf files to understand the code base

* Open the variables.tf file and replace the placeholder in the key_name variable with an appropriate name

* Create a new variable for key category and add this variable to the aws_kms_alias resource block in the main.tf

* Create a 'outputs.tf' file and create 2 outputs for your KMS key. 1 output for the key id and 1 output for the key arn

* Run 'terraform init' to initialize your files

* Run 'terraform plan' to see the resources that you are going to create

* Run 'terraform apply' to deploy your resources

* After the successful apply, go on the AWS Console to find your KMS key

Helpful Links:

* Variables: https://www.terraform.io/docs/language/values/variables.html

* KMS Key: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key

* KMS Alias: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias

* Outputs: https://www.terraform.io/docs/language/values/outputs.html


## Part 2 - Resources & Outputs

A new JIRA ticket with ticket number CE-456 has been assigned to you. The ticket entails that you need to create an S3 bucket for storing data. It should also be encrypted using KMS. Deploy an encrypted bucket to the account to complete and close your JIRA ticket. 

### Steps 

* In the main.tf file, declare a resource block to create an S3 bucket

* Create a new variable for your bucket name

* Update you S3 resource block to enable server side encryption and use the output from the key that you created in part 1 to encrypt your bucket

* Update your outputs.tf file and create 2 outputs for S3. 1 output for the bucket id and 1 output for the bucket arn

* Run 'terraform init' to initialize your files

* Run 'terraform plan' to see the resources that you are going to create

* Run 'terraform apply' to deploy your resources

* After the successful apply, go on the AWS Console to check if your KMS key and your encrypted bucket has been deployed correctly

Helpful Links:

* Resources: https://www.terraform.io/docs/language/resources/syntax.html

* S3 Bucket: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket

* S3 Bucket Policy: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy

* Policy Document Data Sources: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document 


# After completing this exercise, please make sure to run 'terraform destroy' to destroy your resources