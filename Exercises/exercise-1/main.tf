###########
# KMS Key #
###########
resource "aws_kms_key" "key" {
  description             = "${var.key_name} encryption key"
  is_enabled              = true
  deletion_window_in_days = 7
  enable_key_rotation     = true
  tags                    = local.mandatory_tags
}

#################
# KMS Key Alias #
#################
resource "aws_kms_alias" "name" {
  name          = "alias/${var.key_name}"
  target_key_id = aws_kms_key.key.key_id
}

##########################
# Create S3 Bucket Below #
##########################


