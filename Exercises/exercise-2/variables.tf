################
# S3 Variables #
################
variable "bucket_name" {
  description = "Name of your bucket"
  type        = string
  default     = "PLACEHOLDER"
}