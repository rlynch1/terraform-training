# Exercise 2 - S3 Backend and Data Sources

Ensure that you have logged into the AWS account before you start the exercise.

## S3 Backend

You are currently storing your Terraform state file on your local machine. Your project manager has decided that the team needs to store the state files remotely in S3 for better collaboration. The name of the bucket where you need to store your state file in is 'training-ce-terraform-state-bucket'.

Deploy a simple security group and store the state file remotely in S3 for testing purposes. You can create an empty security group with no rules.

### Steps 

* Ensure that you are in the exercise-2 directory within your command line interface

* In your IDE, open the backend.tf file to understand the structure of the code for remote backends

* Replace the placeholder in the backend.tf file with a unique string e.g your name

* Open the security group link below to find the appropriate code to write

* Open the main.tf file and write the appropriate resource block which creates a security group

* Create a variable for your security group name

* Run 'terraform init' to initialize your files

* Run 'terraform plan' to see the resources that you are going to create

* Run 'terraform apply' to deploy your resources

* After the successful apply, go on the AWS Console to check if your security group has been deployed and check the S3 bucket in your respective path to see your state file

Helpful Links:

* Security group: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group

* S3 Backend: https://www.terraform.io/docs/language/settings/backends/s3.html

* State files: https://www.terraform.io/docs/language/state/index.html


## Data Sources

Ticket number CE-789 has been assigned to you. The ticket entails that you need to create another S3 bucket and encrypt it with the master data encryption key which the client has already deployed to the account. 

You have been informed that the name of the key that you should use is 'alias/data-encryption/account-key'. 

It also says that your bucket must have a single lifecycle rule whereby objects must be automatically moved to Infrequently Accessed after 3 months and then moved to Glacier after 6 months.

Deploy the new bucket and encrypt it with the client KMS key. Also check that the correct lifecycle rule has been applied to your bucket to successfully complete and close your JIRA ticket. 

### Steps 

* Ensure that you are in the exercise-2 directory within your command line interface

* Declare the resource block to create an S3 bucket in the main.tf file and use the bucket_name variable for your bucket

* Open the KMS key data sources link below to find examples of data sources

* Populate the aws_kms_key data block in the data.tf file. You should populate it by using the key ARN

* Open the main.tf file and update your S3 resource block to enable encryption

* Populate the encryption parameters with the output from the aws_kms_key data source

* Update the S3 resource block to create a lifecycle policy with the required rules from the client

* Run 'terraform init' to initialize your files

* Run 'terraform plan' to see the resources that you are going to create

* Run 'terraform apply' to deploy your resources

* After the successful apply, go on the AWS Console to check that your bucket has been deployed with the correct lifecycle policy and is encrypted with the client's KMS key


Helpful Links:

* S3 Bucket: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket

* Data sources: https://www.terraform.io/docs/language/data-sources/index.html

* KMS Key data source: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/kms_key

* Glacier: https://aws.amazon.com/glacier/#:~:text=The%20Amazon%20S3%20Glacier%20and,separated%20within%20an%20AWS%20Region

* Lifecycle rules: https://docs.aws.amazon.com/AmazonS3/latest/dev/object-lifecycle-mgmt.html


# After completing this exercise, please make sure to run 'terraform destroy' to destroy your resources