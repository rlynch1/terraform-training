##############
# S3 Backend #
##############
# Only replace the placeholder
terraform {
  backend "s3" {
    bucket     = "training-ce-terraform-state-bucket"
    encrypt    = true
    kms_key_id = "arn:aws:kms:eu-west-2:018515648733:key/0e26bdb4-94eb-41b4-8c47-de49f742a692"
    key        = "ce-terraform-training/exercise-2/PLACEHOLDER/terraform.tfstate"
    region     = "eu-west-2"
  }
}
