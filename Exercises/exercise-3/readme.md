# Exercise 3 - Modules

You have been assigned a task to deploy a Posgres Database instance using a Terraform module that the client has already written. You will be working with the client to make improvements to the module so it is important that you understand the current RDS code base.

Deploy a database instance to complete your task. Ensure that you have logged into the AWS account before you start this exercise.

## Steps 

* Ensure that you are in the exercise-3 directory within your command line interface

* In your IDE, open the .tf and readme files in exercise-3/modules/rds to understand the RDS Terraform module

* Replace the placeholder in the exercise-3/backend.tf file with a unique string e.g your name

* Open the exercise-3/main.tf file and replace the placeholders with the appropriate parameters

* Run 'terraform init' to initialize your files

* Run 'terraform plan' to see the resources that you are going to create

* Run 'terraform apply' to deploy your resources

* After the successful apply, go on the AWS Console to check if the module resources has been deployed correctly. You script should have deployed a security group, a db subnet group, an RDS instance, a KMS key, and a secret in Secrets Manager

* Once you have checked your DB in the console, run 'terraform destroy' to delete your resources

## Extra Task

Currently, some values are hardcoded in the module which means that the module only allows deployments of the Posgres db engine. 

You have been asked to modify the module such that it allows the deployment of other DB engines. 

* The user should be able to supply the db engine, engine version, and the instance class.

* The user should also be able to update and supply the db storage type, allocated storage and max allocated storage.

* After making the changes, deploy a database of your choice(excluding Posgres) and update the database storage to test your changes.


Helpful Links:

* Modules: https://www.terraform.io/docs/language/modules/index.html

* Module Sources: https://www.terraform.io/docs/language/modules/sources.html

* Module Blocks: https://www.terraform.io/docs/language/modules/syntax.html

* Variables: https://www.terraform.io/docs/language/values/variables.html

* DB Instance: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance

* Secrets Manager: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret


# After completing this exercise, please make sure to run 'terraform destroy' to destroy your resources
