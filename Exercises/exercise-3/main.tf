#####################
# Local Definitions #
#####################
# Don't update
locals {
  time          = timestamp()
  creation_date = formatdate("DD MMM YYYY hh:mm ZZZ", local.time)

  mandatory_tags = map(
    "CREATION-DATE", local.creation_date,
    "CREATED-BY", "TERRAFORM",
    "DEPLOYED-BY", data.aws_caller_identity.current.user_id
  )
}

#############
# Create DB #
#############
# Declare the module call below to create your database
module "posgres_db" {
  source = "./modules/rds"

  db_name     = "PLACEHOLDER"
  db_username = "PLACEHOLDER"
  db_password = "PLACEHOLDER"
}