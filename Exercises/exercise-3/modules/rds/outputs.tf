##############
# DB Outputs #
##############
output "database_arn" {
  value = aws_db_instance.primary.arn
}

output "database_host" {
  value = aws_db_instance.primary.address
}

output "database_port" {
  value = aws_db_instance.primary.port
}

output "database_endpoint" {
  value = aws_db_instance.primary.endpoint
}

output "database_name" {
  value = aws_db_instance.primary.name
}

output "database_identifier" {
  value = aws_db_instance.primary.identifier
}

output "secret_arn" {
  value = aws_secretsmanager_secret.secret.arn
}
