# RDS Module

Terraform RDS module that creates a Postgres instance in your private subnets. Database access details are then stored in Secrets Manager

## Mandatory Inputs
The parameters below are the required inputs for the module

| Name | Description | Type | 
|------|-------------|:----:|
| db_name | Your database name | String | 
| db_username | Username for the master DB user | String | 
| db_password | Password for the master DB user. Note that this may show up in logs, and it will be stored in the state file | String | 

## Optional Inputs
The parameters below are the optional inputs for the module which can be overwritten

| Name | Description | Type | Default |
|------|-------------|:----:| :----:|
|db_backup_window |The daily time range (in UTC) during which automated backups are created if they are enabled. Must not overlap with maintenance_window | String | 03:30-04:30 |
|db_backup_retention_period | The days to retain backups for. Must be between 0 and 35. Must be greater than 0 if the database is used as a source for a Read Replica | Number | 10 |
|db_snapshot_skip_on_deletion | Determines whether a final DB snapshot is created before the DB instance is deleted.| Bool | true |
|db_apply_immediately |Specifies whether any database modifications are applied immediately, or during the next maintenance window. | Bool |true |
|db_maintenance_window | The window to perform maintenance in. Syntax: ddd:hh24:mi-ddd:hh24:mi.| String | Sun:00:00-Sun:03:00 |
|db_deletion_protection|If the DB instance should have deletion protection enabled. The database can't be deleted when this value is set to true. | Bool | false|

## Example Usage

```hcl
module "posgres_db" {
  source = "../modules/rds"

  db_name     = "mydatabase"
  db_username = "username"
  db_password = "password"
}
```
