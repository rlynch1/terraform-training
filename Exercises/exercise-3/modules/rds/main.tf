####################################
# RDS DATABASE MODULE - Create DBs #
####################################
# 1. Create Security Group
# 2. Create Database Pre-Requisistes
# 3. Create Database

############################
# 1. Create Security Group #
############################
resource "aws_security_group" "db_security_group" {
  name        = "${var.db_name}-db-sg"
  description = "SG for ${var.db_name} database"
  vpc_id      = data.aws_vpc.vpc_id.id
  tags        = local.mandatory_tags

  ingress {
    from_port   = var.db_port
    to_port     = var.db_port
    protocol    = "tcp"
    description = "Database ingress rule for ${var.db_name}"
    cidr_blocks = data.aws_subnet.subnets.*.cidr_block
  }
}

######################################
# 2. Create Database Pre-Requisistes #
######################################
resource "aws_db_subnet_group" "subnet_group" {
  name       = "db-${var.db_name}"
  subnet_ids = tolist(data.aws_subnet_ids.vpc_private_subnets.ids)
  tags       = local.mandatory_tags
}

######################
# 3. Create Database #
######################
resource "aws_db_instance" "primary" {

  # Database Type / Setup
  engine               = "postgres"
  engine_version       = "10.7"
  instance_class       = "db.t2.medium"
  identifier           = var.db_name
  name                 = var.db_name
  tags                 = local.mandatory_tags

  # Connection Details
  username = var.db_username
  password = var.db_password
  port     = var.db_port

  # Database Storage
  storage_type          = "gp2"
  allocated_storage     = 20
  max_allocated_storage = 150

  # Encryption
  storage_encrypted = true
  kms_key_id        = aws_kms_key.db_key.arn

  # Backups / Snapshots
  backup_window           = var.db_backup_window
  backup_retention_period = var.db_backup_retention_period
  skip_final_snapshot     = var.db_snapshot_skip_on_deletion
  copy_tags_to_snapshot   = true

  # Updates / Maintenance
  apply_immediately   = var.db_apply_immediately
  maintenance_window  = var.db_maintenance_window
  deletion_protection = var.db_deletion_protection

  # Network / Security
  vpc_security_group_ids = [aws_security_group.db_security_group.id]
  db_subnet_group_name   = aws_db_subnet_group.subnet_group.name
  multi_az               = false
  ca_cert_identifier     = "rds-ca-2019"

  depends_on = [
    aws_security_group.db_security_group,
    aws_db_subnet_group.subnet_group
  ]
}
