#####################
# Local Definitions #
#####################
# Don't update
locals {
  mandatory_tags = map(
    "CREATED-BY", "TERRAFORM",
    "DEPLOYED-BY", data.aws_caller_identity.current.user_id
  )
}