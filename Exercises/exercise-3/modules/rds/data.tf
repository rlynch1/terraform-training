##-- General AWS --##
data "aws_partition" "current" {}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

##-- Get VPC --##
data "aws_vpc" "vpc_id" {
  filter {
    name   = "tag:Name"
    values = ["ce-terraform-vpc"]
  }
}

##-- Get Private Subnet ID's --##
data "aws_subnet_ids" "vpc_private_subnets" {
  vpc_id = data.aws_vpc.vpc_id.id
  filter {
    name = "tag:Name"
    values = [
      "private-subnet*",
    ]
  }
}

##-- Get Private Subnets --##
data "aws_subnet" "subnets" {
  count = length(data.aws_subnet_ids.vpc_private_subnets.ids)
  id    = element(tolist(data.aws_subnet_ids.vpc_private_subnets.ids), count.index)
}
