##############
# Encryption #
##############
# 1. Create KMS Key
# 2. Create Secrets Manager
# 3. Store Credentials in Secrets Manager

##############
# 1. KMS Key #
##############
resource "aws_kms_key" "db_key" {
  description             = "Encryption key for ${var.db_name}"
  is_enabled              = true
  deletion_window_in_days = 7
  enable_key_rotation     = true
  tags                    = local.mandatory_tags
}

resource "aws_kms_alias" "secret" {
  name          = "alias/service-encryption/${var.db_name}"
  target_key_id = aws_kms_key.db_key.key_id
}

######################
# 2. Secrets Manager #
######################
resource "aws_secretsmanager_secret" "secret" {
  name                    = "${var.db_name}-db-credentials"
  description             = "Database connection and access details for ${var.db_name}"
  kms_key_id              = aws_kms_key.db_key.key_id
  recovery_window_in_days = 0

  tags = merge(
    local.mandatory_tags,
    {
      "source_db" = lower(aws_db_instance.primary.name)
    },
  )
}

###########################################
# 3. Store Credentials in Secrets Manager #
###########################################
resource "aws_secretsmanager_secret_version" "secret_value" {
  lifecycle {
    ignore_changes = [secret_string]
  }
  secret_id     = aws_secretsmanager_secret.secret.id
  secret_string = <<EOF
{
  "username": ${aws_db_instance.primary.username},
  "engine":   ${aws_db_instance.primary.engine},
  "dbname":   ${aws_db_instance.primary.name},
  "host":     ${aws_db_instance.primary.address},
  "password": ${aws_db_instance.primary.password},
  "port":     ${aws_db_instance.primary.port}
}
EOF
}
