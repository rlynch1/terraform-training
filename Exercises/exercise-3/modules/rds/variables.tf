######################
# DATABASE VARIABLES #
######################

# General Variables
variable "db_name" {
  description = "Your database name"
  type        = string
}

variable "db_username" {
  description = "Username for the master DB user"
  type        = string
}

variable "db_password" {
  description = "Password for the master DB user. Note that this may show up in logs, and it will be stored in the state file"
  type        = string
}

variable "db_port" {
  description = "The port on which the DB accepts connections"
  type        = number
  default     = 5432
}

# Backups / Snapshots
variable "db_backup_window" {
  description = "The daily time range (in UTC) during which automated backups are created if they are enabled. Must not overlap with maintenance_window"
  type        = string
  default     = "03:30-04:30"
}

variable "db_backup_retention_period" {
  description = "The days to retain backups for. Must be between 0 and 35. Must be greater than 0 if the database is used as a source for a Read Replica"
  type        = number
  default     = 10
}

variable "db_snapshot_skip_on_deletion" {
  description = "Determines whether a final DB snapshot is created before the DB instance is deleted."
  type        = bool
  default     = true
}

# Updates / Maintenance
variable "db_apply_immediately" {
  description = "Specifies whether any database modifications are applied immediately, or during the next maintenance window."
  type        = bool
  default     = true
}

variable "db_maintenance_window" {
  description = "The window to perform maintenance in. Syntax: ddd:hh24:mi-ddd:hh24:mi."
  type        = string
  default     = "Sun:00:00-Sun:03:00"
}

variable "db_deletion_protection" {
  description = "If the DB instance should have deletion protection enabled. The database can't be deleted when this value is set to true."
  type        = bool
  default     = false
}
