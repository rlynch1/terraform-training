# Exercise 4 - Creating Modules

## Summary

So far you have learned how to read, write and use variables, outputs, resources, data sources and modules.

In this exercise, you will be using what you have learned so far and you will be writing two modules from scratch.

### Scenario

The client has asked you to help out with a POC. They want learn how to use AWS Simple Notification Service(SNS). They want you to help with creating a Terraform module for SNS and KMS keys for encryption.

You are only responsible for creating the modules. After you create the modules, you will be handing the code base to a different team who will be testing the integration of SNS with other AWS services.

Note: KMS keys were baked into the RDS module in the previous exercise. You have to create a separate module for KMS in this exercise to make the module reusable and generic. The end goal is to have a modules folder which will have a folder for KMS and another folder for SNS.


### Part 1 - Create KMS Key Module

Requirements:

* The module should be able to deploy a key with an alias

* The key should include a category in its name e.g alias/app-encryption/mykey, alias/data-encryption/mykey, alias/service-encryption/mykey

* The user should be able to define the description, tags, key policy, deletion window in days and enable or disable rotation of the key

* To ensure that the keys can be integrated and used by other AWS services, the module should have the following outputs:
   * key_arn
   * key_id 

Helpful Links:

* https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key

* https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias 


### Part 2 - Create SNS Module

Requirements:

* The module should be able to deploy an SNS topic

* The user should be able to define the name, tags and the kms encryption key id which should be mandatory

* To ensure that the sns topic can be integrated with other AWS services, the module should have the following outputs:
    * id

Helpful Links:

* https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic


### Part 3 - Deployment and Testing

1. Store your state file in an S3 backend. Give it a unique key to ensure that you're not overwriting another person's state file

2. Deploy a KMS Key with an appropriate name and tag it with your name

3. Deploy an SNS topic, tag it with your name and encrypt it with the key from the previous step by using the KMS key module output


# After completing this exercise, please make sure to run 'terraform destroy' to destroy your resources