
// Variable declaration to re use in the code
variable "vpc_cidr" {
  description = "vpcc cidr address"
  type        = string
  default     = "10.1.0.0/16"
}

variable "vpc_tagname" {
  description = "vpc tag name"
  type = string
  default = "TF-Sample-vpc"
}
