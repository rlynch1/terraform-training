// Provider section to define the Cloud Provider to build as AWS/Azure/Google
provider "aws" {
  version = "~> 3.0"
  region  = "eu-west-2"
}

# //Defining the backend for the tfsate file to AWs S3 bucket
# terraform {
#   backend "s3" {
#     bucket = "training-ce-terraform-state-bucket/ce-terraform-training/training/"
#     key    = "vpc-tfstate"
#     region = "eu-west-2"
#   }
# }