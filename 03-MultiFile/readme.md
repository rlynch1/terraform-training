# Multi file approach
Terraform scripts can be split into multiple files to make it easier to read and understand. The best practice approach is to use the following: 

- main.tf file: This is where resource blocks are declared
- variables.tf file: This is where variables blocks are declared
- providers.tf file: This is where providers blocks are declared
- outputs.tf file: This is where outputs are declared
- backend.tf file: This is where state file backend configuration is declared
- data.tf file: This is where data sources are declared
- terraform.tfvars file: This is where variable values defined 

Files can be customisable aslong as they are suffixed with '.tf'. If your script is complex, you can also group resources together. For example, if you have a script for a Microservices blueprint, you could have a s3.tf file for all your buckets, eks.tf file for all your cluster configuration, rds.tf for your databases etc.

.tfvars file name should be 'terraform' for terraform to auto recognise the file for build. Alternatively you can specify as ‘filename.auto.tfvars’ or call in the file during terraform apply ‘terraform apply -var-file=filename.tfvars.

However, if you have a long and complex script, you should consider breaking your code up into modules to reduce the length of your code and to make it easier to manage. 
