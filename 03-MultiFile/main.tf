

// Resource section to define the resource to be created in the cloud with variables 
resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr
  instance_tenancy = "default"

tags = {
  Name = var.vpc_tagname
  }

}


