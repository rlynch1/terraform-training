provider "aws" {
  version = "~> 3.0"
  region  = "eu-west-2"
}

variable "vpc_id" {}

data "aws_vpc" "vpc" {
    id = var.vpc_id
}

output "vpc_id" {
  value = data.aws_vpc.vpc.arn
}