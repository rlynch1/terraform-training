
// Provider section to define the Cloud Provider to build as AWS/Azure/Google
provider "aws" {
  version = "~> 3.0"
  region  = "eu-west-2"
}


// Resource section to define the resource to be created in the cloud 

resource "aws_vpc" "vpc" {
  cidr_block = "10.1.0.0/16"
  instance_tenancy = "default"
tags = {
  Name = "TF-Sample-VPC"
  }

}



// Output section to call out the build resource specific values 
output "vpc_id" {
  value = aws_vpc.vpc.id
}
