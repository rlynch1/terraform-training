# Helpful Links

## Resources
Resource declaration can be simple or complex depending on your requirement. Depending on the service/resource, you only need to provide a small subset of values for initial use. Default values are used if the parameter is not populated and there are advanced features which are optional. See the link below for more information regarding Terraform resource blocks.

- https://www.terraform.io/docs/configuration/blocks/resources/syntax.html

## Providers
Providers can be seen as plugins to interact with other systems. Your Terraform scripts must declare which providers to use so that Terraform can install and use them. For example, declaring a provider for AWS allows you to write scripts to deploy AWS resources and data sources and declaring a provider for Azure allows you to write scripts for Azure. See the link below for more information regarding Terraform provider blocks.

- https://www.terraform.io/docs/configuration/blocks/providers/index.html

## Outputs
Output blocks allow you to extract values from a Terraform resource which you can then use in other parts of your scripts. For example, if you have a resource block which creates an EC2 instance, you can define an output block in your code to extract the instance ID or instance ARN which you can then use elsewhere in your script. See the link below for more information regarding Terraform output blocks.

- https://www.terraform.io/docs/configuration/outputs.html