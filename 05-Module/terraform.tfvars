
//Variable Declaration for VPC1

vpc1_cidr = "10.1.0.0/16"

vpc1_tag = "TF-Sample-VPC-1"



//Variable Declaration for VPC1 subnet

subnet1_cidr = "10.1.1.0/24"



//Variable Declaration for VPC2

vpc2_cidr = "10.2.0.0/16"

vpc2_tag = "TF-Sample-VPC-2"
