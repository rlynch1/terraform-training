// Provider section to define the Cloud Provider AWS/Azure/Google
provider "aws" {
  version = "~> 3.0"
  region  = "eu-west-2"
}