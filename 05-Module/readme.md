# Helpful Links

## Modules
Modules can be seen as containers or blueprints. It is a collection of files that are kept together in a directory. Creating modules for services allow you to reuse the source code without having to write everything from scratch and you can also write modules in such ways to ensure that your code will always meet security or quality requirements. See the link below for more information regarding Terraform modules.

- https://www.terraform.io/docs/configuration/blocks/modules/index.html