
resource "aws_subnet" "vpc_subnet" {
    vpc_id = var.vpc_id
    cidr_block = var.subnet_cidr
  }