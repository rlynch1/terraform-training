resource "aws_vpc" "tf_vpc" {
  cidr_block = var.vpc_cidr
  instance_tenancy = "default"

  tags = {
      Name = var.vpc_tagname
  }
}
