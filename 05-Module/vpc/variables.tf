variable "vpc_cidr" {
    description = "VPC CIDR value"
    type = string
}

variable "vpc_tagname" {
    description = "vpc tag name"
    type = string
}