
//Variable Declaration for VPC1

variable "vpc1_cidr" {
    description = "VPC CIDR value"
    type = string
    default = "10.1.0.0/16"
}

variable "vpc1_tag" {
    description = "VPC tag name"
    type = string
    default = "TF-Sample-VPC-1"
}


//Variable Declaration for VPC1 subnet

variable "subnet1_cidr" {
  description = "subnet cidr"
  type = string
  default = "10.1.1.0/24"
}


//Variable Declaration for VPC2

variable "vpc2_cidr" {
    description = "VPC CIDR value"
    type = string
    default = "10.2.0.0/16"
}

variable "vpc2_tag" {
    description = "VPC tag name"
    type = string
    default = "TF-Sample-VPC-2"
}