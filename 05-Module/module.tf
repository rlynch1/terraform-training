// Calls the local vpc module in vpc/main.tf for first vpc

module "vpc1" {
  source = "./vpc"  
  vpc_cidr = var.vpc1_cidr
  vpc_tagname = var.vpc1_tag
}


// Calls the local subnet module in subnet/main.tf
module "subnet" {
  source = "./subnet"
  vpc_id = module.vpc1.vpc_id
  subnet_cidr = var.subnet1_cidr
  }


// Calls the local vpc module in vpc/main.tf for second vpc

module "vpc2" {
  source = "./vpc"  
  vpc_cidr = var.vpc2_cidr
  vpc_tagname = var.vpc2_tag
}




# // Calls vpc module from remote Terrafrom registry moduels
# module "vpc_remote" {
#   source = "terraform-aws-modules/vpc/aws"
#   name = "tf-remote-vpc3"
#   cidr = "10.3.0.0/16"

#    tags = {
#     Name = "tf-remote-vpc3"
#   }
# }