provider "aws" {
  version = "~> 3.0"
  region  = "eu-west-2"
}

resource "aws_s3_bucket" "bucket" {
  bucket = "ce-tf-filefunction-bucket"
}

// Calling in the policy json file 
data "template_file" "access_policy" {
  template = file("policy.json")

  vars = {
    bucket_arn = aws_s3_bucket.bucket.arn
  }
}

//assigning the policy from file to the bucket
resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.bucket.id
  policy = data.template_file.access_policy.rendered
}