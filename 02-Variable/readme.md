# Helpful Links

## Variables
Variables serve as parameters for a Terraform module which allows you to customise different parts of the module without altering the root module code. When you declare variables in child modules, the calling module should pass the values in the module block. See the link below for more information regarding Terraform variable blocks.

- https://www.terraform.io/docs/configuration/variables.html
