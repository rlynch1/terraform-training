
// Provider section to define the Cloud Provider to build as AWS/Azure/Google
provider "aws" {
  version = "~> 3.0"
  region  = "eu-west-2"
}


// Variable declaration to re use in the code
variable "vpc_cidr" {
  description = "vpcc cidr address"
  type        = string
  default     = "10.1.0.0/16"
}

variable "vpc_tagname" {
  description = "vpc tag name"
  type = string
  default = "TF-Sample-vpc"
}

// Resource section to define the resource to be created in the cloud with variables 
resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr
  instance_tenancy = "default"

tags = {
  Name = var.vpc_tagname
  }

}

// Output section to call out the build resource specific values 
output "vpc_id" {
  value = aws_vpc.vpc.id
}
